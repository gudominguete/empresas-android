package com.example.gudominguete.teste_ioasys.controllers;

import com.example.gudominguete.teste_ioasys.beans.ListagemEmpresaDTO;

import retrofit2.Call;
import retrofit2.http.GET;

public interface EmpresaController {

    @GET("enterprises")
    Call<ListagemEmpresaDTO> listarEmpresas();
}
