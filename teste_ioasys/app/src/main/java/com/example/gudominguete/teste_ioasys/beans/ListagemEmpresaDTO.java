package com.example.gudominguete.teste_ioasys.beans;

import com.example.gudominguete.teste_ioasys.model.Empresa;

import java.util.List;

public class ListagemEmpresaDTO {

    private List<Empresa> enterprises;

    public List<Empresa> getEnterprises() {
        return enterprises;
    }

    public void setEnterprises(List<Empresa> enterprises) {
        this.enterprises = enterprises;
    }
}
