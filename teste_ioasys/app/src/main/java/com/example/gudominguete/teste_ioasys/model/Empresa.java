package com.example.gudominguete.teste_ioasys.model;

public class Empresa {
    private Long id;
    private String email_enterprise;
    private String facebook;
    private String twitter;
    private String linkedin;
    private String phone;
    private Boolean own_enterprise;
    private String enterprise_name;
    private String photo;
    private String description;
    private String city;
    private String country;
    private Long value;
    private Long share_price;
    private TipoEmpresa enterprise_type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail_enterprise() {
        return email_enterprise;
    }

    public void setEmail_enterprise(String email_enterprise) {
        this.email_enterprise = email_enterprise;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Boolean getOwn_enterprise() {
        return own_enterprise;
    }

    public void setOwn_enterprise(Boolean own_enterprise) {
        this.own_enterprise = own_enterprise;
    }

    public String getEnterprise_name() {
        return enterprise_name;
    }

    public void setEnterprise_name(String enterprise_name) {
        this.enterprise_name = enterprise_name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public Long getShare_price() {
        return share_price;
    }

    public void setShare_price(Long share_price) {
        this.share_price = share_price;
    }

    public TipoEmpresa getEnterprise_type() {
        return enterprise_type;
    }

    public void setEnterprise_type(TipoEmpresa enterprise_type) {
        this.enterprise_type = enterprise_type;
    }
}
