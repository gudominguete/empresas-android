package com.example.gudominguete.teste_ioasys.services;

import com.example.gudominguete.teste_ioasys.controllers.EmpresaController;
import com.example.gudominguete.teste_ioasys.controllers.SecureController;
import com.example.gudominguete.teste_ioasys.model.Configuration;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class EmpresaService {

    private static Retrofit retrofit;

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    public static EmpresaController createService() {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .header("access-token", Configuration.access_token)
                        .header("client", Configuration.client)
                        .header("uid", Configuration.uid)
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();
        retrofit = new Retrofit.Builder()
                .baseUrl("http://empresas.ioasys.com.br/api/v1/")
                .addConverterFactory(JacksonConverterFactory.create())
                .client(client)
                .build();

        return retrofit.create(EmpresaController.class);
    }
}
