package com.example.gudominguete.teste_ioasys.model;

import java.util.List;

public class Portifolio {

    private Long enterprises_number;
    private List<Empresa> enterprises;

    public Long getEnterprises_number() {
        return enterprises_number;
    }

    public void setEnterprises_number(Long enterprises_number) {
        this.enterprises_number = enterprises_number;
    }

    public List<Empresa> getEnterprises() {
        return enterprises;
    }

    public void setEnterprises(List<Empresa> enterprises) {
        this.enterprises = enterprises;
    }
}
