package com.example.gudominguete.teste_ioasys.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

import com.example.gudominguete.teste_ioasys.R;
import com.example.gudominguete.teste_ioasys.beans.LoginDTO;
import com.example.gudominguete.teste_ioasys.controllers.SecureController;
import com.example.gudominguete.teste_ioasys.model.Configuration;
import com.example.gudominguete.teste_ioasys.services.SecureService;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends Activity {

    @BindView(R.id.email)
    EditText emailET;

    @BindView(R.id.senha)
    EditText senhaET;

    private Context context;
    private Configuration configuration;

    private Retrofit retrofit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        context = this.getApplicationContext();
        configuration = new Configuration();

    }

    public void doLogin(View view){
        String email = this.emailET.getText().toString();
        String senha = this.senhaET.getText().toString();

        SecureController retrofit = SecureService.createService();

        Call<LoginDTO> responseBodyCall = retrofit.autenticacao(email, senha);


        responseBodyCall.enqueue(obterDadosAutenticacaoCallback());
    }

    private Callback<LoginDTO> obterDadosAutenticacaoCallback() {
        return new Callback<LoginDTO>() {

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onResponse(Call<LoginDTO> call, Response<LoginDTO> response) {
                if(response.code() == 200){
                    Log.i("auth", "deu certo");
                    configuration.usuario = response.body().getInvestor();
                    configuration.access_token = response.headers().get("access-token");
                    configuration.client = response.headers().get("client");
                    configuration.uid  = response.headers().get("uid");

                    Intent intent =  new Intent(context, ListagemEmpresaActivity.class);
                    startActivity(intent);

                    Toast.makeText(context, "Autenticado!", Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(context, "Login ou senha errados!", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<LoginDTO> call, Throwable t) {
                t.printStackTrace();

            }
        };
    }
}
