package com.example.gudominguete.teste_ioasys.controllers;

import com.example.gudominguete.teste_ioasys.beans.LoginDTO;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface SecureController {

    @POST("users/auth/sign_in")
    Call<LoginDTO> autenticacao(@Query("email") String email, @Query("password") String senha);
}

