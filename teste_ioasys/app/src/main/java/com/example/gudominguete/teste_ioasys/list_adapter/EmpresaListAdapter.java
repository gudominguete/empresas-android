package com.example.gudominguete.teste_ioasys.list_adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.gudominguete.teste_ioasys.R;
import com.example.gudominguete.teste_ioasys.model.Empresa;

import java.util.List;

public class EmpresaListAdapter extends BaseAdapter {

    private final List<Empresa> empresas;
    private Activity act;

    public EmpresaListAdapter(List<Empresa> empresas, Activity act) {
        this.empresas = empresas;
        this.act = act;
    }

    @Override
    public int getCount() {
        return empresas.size();
    }

    @Override
    public Object getItem(int i) {
        return empresas.get(i);
    }

    @Override
    public long getItemId(int i) {
        return empresas.get(i).getId();
    }

    @Override
    public View getView(int i, View contentView, ViewGroup viewGroup) {
         View view = act.getLayoutInflater()
                .inflate(R.layout.list_view_empresas, viewGroup, false);
         Empresa empresa = empresas.get(i);

         TextView nome = (TextView)
                view.findViewById(R.id.nome);
         TextView tipoNegocio = (TextView)
                view.findViewById(R.id.tipo_negocio);
         TextView pais = (TextView)
                view.findViewById(R.id.pais);

         nome.setText(empresa.getEnterprise_name());
         tipoNegocio.setText(empresa.getEnterprise_type().getEnterprise_type_name());
         pais.setText(empresa.getCountry());
         return view;
    }
}
