package com.example.gudominguete.teste_ioasys.model;

public class Usuario {

    private Long id;
    private String investor_name;
    private String email;
    private String city;
    private String country;
    private Long balance;
    private String photo;
    private Portifolio portfolio;
    private Long portfolio_value ;
    private Boolean first_access;
    private Boolean super_angel;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInvestor_name() {
        return investor_name;
    }

    public void setInvestor_name(String investor_name) {
        this.investor_name = investor_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Portifolio getPortfolio() {
        return portfolio;
    }

    public void setPortfolio(Portifolio portfolio) {
        this.portfolio = portfolio;
    }

    public Long getPortfolio_value() {
        return portfolio_value;
    }

    public void setPortfolio_value(Long portfolio_value) {
        this.portfolio_value = portfolio_value;
    }

    public Boolean getFirst_access() {
        return first_access;
    }

    public void setFirst_access(Boolean first_access) {
        this.first_access = first_access;
    }

    public Boolean getSuper_angel() {
        return super_angel;
    }

    public void setSuper_angel(Boolean super_angel) {
        this.super_angel = super_angel;
    }
}
