package com.example.gudominguete.teste_ioasys.beans;

import com.example.gudominguete.teste_ioasys.model.Usuario;

public class LoginDTO {

    private Usuario investor;
    String enterprise;
    Boolean success;

    public Usuario getInvestor() {
        return investor;
    }

    public void setInvestor(Usuario investor) {
        this.investor = investor;
    }

    public String getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(String enterprise) {
        this.enterprise = enterprise;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
