package com.example.gudominguete.teste_ioasys.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.widget.ListView;
import android.widget.Toast;

import com.example.gudominguete.teste_ioasys.R;
import com.example.gudominguete.teste_ioasys.beans.ListagemEmpresaDTO;
import com.example.gudominguete.teste_ioasys.beans.LoginDTO;
import com.example.gudominguete.teste_ioasys.controllers.EmpresaController;
import com.example.gudominguete.teste_ioasys.controllers.SecureController;
import com.example.gudominguete.teste_ioasys.list_adapter.EmpresaListAdapter;
import com.example.gudominguete.teste_ioasys.model.Empresa;
import com.example.gudominguete.teste_ioasys.services.EmpresaService;
import com.example.gudominguete.teste_ioasys.services.SecureService;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListagemEmpresaActivity extends Activity {

    @BindView(R.id.list_empresa)
    ListView empresaLV;

    Activity act;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_listagem_empresa);
        ButterKnife.bind(this);
        act = this;
        EmpresaController retrofit = EmpresaService.createService();

        Call<ListagemEmpresaDTO> responseBodyCall = retrofit.listarEmpresas();


        responseBodyCall.enqueue(obterDadosListagem());
    }



    private Callback<ListagemEmpresaDTO> obterDadosListagem() {
        return new Callback<ListagemEmpresaDTO>() {

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onResponse(Call<ListagemEmpresaDTO> call, Response<ListagemEmpresaDTO> response) {
                if(response.code() == 200){
                    Log.i("tamanho lista", String.valueOf(response.body().getEnterprises().size()));

                    EmpresaListAdapter adapter =
                            new EmpresaListAdapter(response.body().getEnterprises(), act);

                    empresaLV.setAdapter(adapter);
                }else {

                }

            }

            @Override
            public void onFailure(Call<ListagemEmpresaDTO> call, Throwable t) {
                t.printStackTrace();

            }
        };
    }
}
