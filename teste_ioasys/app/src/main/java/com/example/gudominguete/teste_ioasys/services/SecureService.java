package com.example.gudominguete.teste_ioasys.services;

import com.example.gudominguete.teste_ioasys.controllers.SecureController;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class SecureService {

    private static Retrofit.Builder builder = new Retrofit.Builder().baseUrl("http://empresas.ioasys.com.br/api/v1/")
            .addConverterFactory(JacksonConverterFactory.create());

    private static Retrofit retrofit = builder.build();

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    public static SecureController createService() {


        return retrofit.create(SecureController.class);
    }
}
